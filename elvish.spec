Name:     PACKAGE_NAME
Summary:  SHORT_DESCRIPTION
URL:      https://PROJECT_HOMEPAGE.TLD
License:  LICENSE_ABBREVIATION
Version:  XX
Release:  1%{?dist}
#BuildArch:  noarch    # This tag should only be needed for noarch packages.

Source0:

BuildRequires: ## TODO
# CentOS 7 build environment may not have incorporated some of Fedora's automated defaults and assumptions.
%if 0%{?rhel}  &&  0%{?rhel} <= 7
BuildRequires: ## TODO
%endif

Requires: ## TODO

# CentOS 7 build environment doesn't support Recommends tag.
%if 0%{?fedora}
Recommends: ## TODO
%endif

%description
PARAGRAPH OR TWO OF DESCRIPTION...



%prep
%autosetup -n NAME_OF_UNPACKED_TARBALL_DIRECTORY


%build
%configure
%make_build


%install
%make_install

%find_lang LANG_FILES_NAME



%files -f LANG_FILES_NAME.lang
%doc DOCS_DIRECTORY_OR_README
%license LICENSE_FILE
%{_bindir}/%{name}



%changelog
* Sat Apr 1 1999 Andrew Toskin <andrew@tosk.in> - XX-1
- First working build.
